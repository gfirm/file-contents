<?php

Class ScanDirectory 
{

    public function countFileContents($dir) {
        //get all file paths
        $paths = $this->_scanDir($dir);

        $contents = $this->_getFileContent($paths);

        $counts = array_count_values($contents);

        foreach ($counts as $key => $value) {
            if (array_search($key, $contents) != false) {
                $the_path = array_search($key, $contents);
                $new_array[file_get_contents($the_path)] = $value;
            } else {
                $new_array = array('All files unique');
            }
        }
        return $new_array;
    }

    public function getBigestFileContent($dir) {

        $counted_contents = $this->countFileContents($dir);

        if (is_array($counted_contents) && !empty($counted_contents)){
            $biggest = max($counted_contents);

            foreach($counted_contents as $key => $value){
                if($value == $biggest){
                    $result = $key . ' ' . $value;
                }
            }

            return $result;
        } else {
            return 'almost all same';
        }
        
    }

    private function _scanDir($dir, &$results = array()) {
        //filter hidden file, start with . like .DS_Store etc
        $files = preg_grep('/^([^.])/', scandir($dir));

        foreach($files as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                $results[] = $path;
            } else if($value != "." && $value != "..") {
                $this->_scanDir($path, $results);
            }
        }

        return $results;
    }

    private function _getFileContent($file_paths = array()){
        foreach($file_paths as $path){
            $file_contents[$path] = md5_file($path);
        }

        return $file_contents;
    }
}
