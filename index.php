<!DOCTYPE html>
<html>
    <head>
        <title>Scan File</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <form action="index.php" enctype="multipart/form-data" method="post">
                <label for="file">Put folder / directory here :</label>
                <!--<input type="file" name="contents[]" webkitdirectory mozdirectory msdirectory odirectory directory multiple/>-->
                <input type="text" name="directory" />
                <br>
                <input type="submit" value="Scan File" />
            </form>
        </div>
    </body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['directory'] != '') {
    //including class
    include 'ScanDirectory.php'; 

    $scanner = new ScanDirectory;
    //get all fil contents
    $contents = $scanner->countFileContents($_POST['directory']);
    //count all file contents inside directory
    $biggest_content = $scanner->getBigestFileContent($_POST['directory']);

    if (!empty($contents)){
        echo '<ul>';
        foreach ($contents as $name => $number) {
            if ($number > 1) {
                echo '<li>' . $name . ' ' . $number . '</li>';
            }
        }
        echo '</ul>';
    }

    echo "<br>Biggest number is :" . PHP_EOL . $biggest_content;
}
?>